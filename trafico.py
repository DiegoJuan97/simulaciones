#Practica simpy
import simpy

def car(env):
    while True:
        print('El dia empieza a las  %d horas' % env.now)
        yield env.timeout(5)

        print('Encontraste trafico a las  %d de la mañana' % env.now)
        tranque_mañana = 5
        yield env.timeout(tranque_mañana)

        print('El tranque de la mañana duro hasta las %d ' % env.now)
        yield env.timeout(5)

        print('Volviste a toparte con trafico en la tarde a las %d pm' % env.now)
        yield env.timeout(3)

        print('El tranque de la tarde duro hasta las %d pm' % env.now)
        yield env.timeout(1)

        print('Ahora te encuentras en el tranque de la noche a las %d pm' % env.now)
        yield env.timeout(4)

        print('Has salido del tranque a las %d pm' % env.now)
        yield env.timeout(1)

env = simpy.Environment()
env.process(car(env))
env.run(until=24)